import React, {useState, useEffect} from 'react';

import {useDispatch, useSelector} from "react-redux";
import {fetchSerial} from "../../store/actions";
import {NavLink} from "react-router-dom";
import './Serials.css'

const Serials = () => {

    const dispatch = useDispatch();
    const serial = useSelector(state => state.serials);
    const [search, setSearch] = useState('');


    useEffect(() => {
       dispatch(fetchSerial(search));
    }, [search, dispatch]);

    const onChangeHandler = async (e) => {
        const value = e.target.value;
        setSearch(value)
    };

    return (
        <div>
            <div>
                <h2>Search for TV Shows:</h2>
                <input type="text" onChange={onChangeHandler}/>
            </div>
            <div className='search'>
            {serial.map((item, index) => {
                return (
                    <div  style={{height: '20px'}} key={index}>
                        <NavLink to={`/shows/${item.show.id}`}>{item.show.name}</NavLink>
                    </div>
                )
            })}
            </div>
        </div>

    );
};

export default Serials;