import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchOneShow, fetchSerial} from "../../store/actions";
import {NavLink} from "react-router-dom";

const SerialInfo = (props) => {
    const [info, setInfo] = useState('');

    const id = props.match.params.id;
    const dispatch = useDispatch();
    const oneShow = useSelector(state => state.oneShow);
    const serial = useSelector(state => state.serials);

    useEffect(() => {
        dispatch(fetchOneShow(id));
    }, [id, dispatch]);

    useEffect(() => {
        dispatch(fetchSerial(info));
    }, [info, dispatch]);

    const onChangeHandler = async (e) => {
        const value = e.target.value;
        setInfo(value);
    };
    console.log(oneShow);
    return (
        <div>
            <div className='search'>
            <input type="text" value={info} onChange={onChangeHandler}/>
            {serial.map((item, index) => {
                return (
                    <div  style={{height: '20px'}} key={index} >
                        <NavLink to={`/shows/${item.show.id}`}>{item.show.name}</NavLink>
                    </div>
                )
            })}
            </div>
            {oneShow && <div>
                <div>
                    <img style={{width: '200px', height: 'auto'}} src={oneShow.image.original} alt="serial"/>
                </div>
                <div>
                    <h3>{oneShow.name}</h3>
                    <p>{oneShow.summary}</p>
                </div>
            </div>}
        </div>
    );
};

export default SerialInfo;