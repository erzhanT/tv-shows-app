import {FETCH_SERIAL_FAILURE, FETCH_SERIAL_REQUEST, GET_ONE_SHOW} from "./actions";

const {FETCH_SERIAL_SUCCESS} = require("./actions");


const initialState = {
    serials: [],
    oneShow: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SERIAL_REQUEST: {
            return {...state}
        }
        case FETCH_SERIAL_SUCCESS: {
            return {...state, serials: action.serials}
        }
        case GET_ONE_SHOW:
            return {...state, oneShow: action.data}
        case FETCH_SERIAL_FAILURE: {
            return {...state}
        }
        default:
            return state
    }
}

export default reducer;