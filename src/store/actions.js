import axios from 'axios';

const url = 'http://api.tvmaze.com/'


export const FETCH_SERIAL_REQUEST = 'FETCH_SERIAL_REQUEST';
export const FETCH_SERIAL_SUCCESS = 'FETCH_SERIAL_SUCCESS';
export const FETCH_SERIAL_FAILURE = 'FETCH_SERIAL_FAILURE';
export const GET_ONE_SHOW = 'GET_ONE_SHOW';


export const fetchSerialRequest = () => ({type: FETCH_SERIAL_REQUEST});
export const fetchSerialSuccess = serials => ({type: FETCH_SERIAL_SUCCESS, serials});
export const fetchSerialFailure = () => ({type: FETCH_SERIAL_FAILURE});
export const getOneShowSuccess = data => ({type: GET_ONE_SHOW, data});

export const fetchSerial = (name) => {
    return async dispatch => {
        dispatch(fetchSerialRequest());
        try {
            const response = await axios.get(url + `search/shows?q=${name}`);
            dispatch(fetchSerialSuccess(response.data));
        } catch (e) {
            dispatch(fetchSerialFailure());
        }
    }
};

export const fetchOneShow = (id) => {
    return async dispatch => {
        dispatch(fetchSerialRequest());
        try {
            const response = await axios.get(url + `shows/${id}`);
            dispatch(getOneShowSuccess(response.data));
        } catch (e) {
            dispatch(fetchSerialFailure());
        }
    }
};

