import React from "react";
import './App.css';
import Serials from "./containers/Serials/Serials";
import {Switch, Route} from 'react-router-dom';
import SerialInfo from "./containers/SerialInfo/SerialInfo";
const App = () => (
    <Switch>
        <Route path="/" exact component={Serials} />
        <Route path="/shows/:id" exact component={SerialInfo} />
    </Switch>
);

export default App;
